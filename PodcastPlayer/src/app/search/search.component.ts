import { Component, OnInit } from '@angular/core';
import { SearchResults } from '../models/search-results.model';
import { SearchService } from '../services/search.service';
import { AngularFirestore } from '@angular/fire/firestore';
// import { Test } from '../models/test.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  term: string;
  results: SearchResults;
  constructor(private search: SearchService, private db: AngularFirestore) { }

  ngOnInit() {
    //das
    // const test = this.db.collection('test', ref => ref.where('name', '==', 'google')).valueChanges().subscribe(x => {
    //     console.log(x)
    // });

    // const test = this.db.collection<Test>('test').valueChanges().subscribe(x => {
    //     console.log(x.find(x => x.name == 'google').link)
    // });

    // const test = this.db.collection('test').valueChanges().subscribe(x => {
    //      console.log(x);
    //  });
  }
  submitSearch() {
    console.log('searching');
    this.search.searchPodcast(this.term).subscribe(res => {
      this.results = res;
      console.log(this.results);
    });
  }
}
