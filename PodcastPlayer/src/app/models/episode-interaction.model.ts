export class EpisodeInteraction {
  interaction: Map<number, ActionType>;
  link: number;
}

export class ActionType {
  action: Action;
  endpoint: number;
  url?: string;
  title?: string;
  articleUrl?: string;
}

export enum Action {
  Image,
  Poll,
  Video,
  Links,
  Youtube
}
