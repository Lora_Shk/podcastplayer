import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { PodcastService } from "src/app/services/podcast.service";
import * as firebase from 'firebase';
import { PodcastFeed } from 'src/app/models/podcast-feed.model';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  // feed: PodcastFeed;

  constructor(private db: AngularFirestore, private podcastService: PodcastService, private route: ActivatedRoute,) { }
  
  add(data: any, ref?: any): Promise<firebase.firestore.DocumentReference> {
    const timestamp = this.timestamp;
    // const podcastId = this.podcastId;
    const copyTrans = Object.assign({}, data);
    const data$ = JSON.parse(JSON.stringify(copyTrans));

    return this.db.collection(ref).add({
      ...data$,
      updatedAt: timestamp,
      createdAt: timestamp,
      // podcast: podcastId,
    });
  }

  update<T>(ref: any, data: any): Promise<void> {
    return this.db.doc(ref).update({
      ...data,
      updatedAt: this.timestamp,
    });
  }

  delete<T>(ref: any): Promise<void> {
    return this.db.doc(ref).delete();
  }

  colWithIds$<T>(ref?: any, queryFn?): Observable<any[]> {
    return this.db.collection<T>(ref, queryFn)
      .snapshotChanges()
      .pipe(
        map((actions: DocumentChangeAction<T>[]) => {
          return actions.map((a: DocumentChangeAction<T>) => {
            const data: Object = a.payload.doc.data() as T;
            const docId = a.payload.doc.id;
            const docPath = a.payload.doc.ref.path;
            data['docPath'] = docPath;
            return { docId, docPath, ...data };
          });
        }),
        shareReplay(1)
      );
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  


}

