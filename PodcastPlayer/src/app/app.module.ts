import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCardModule } from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { PlayerComponent} from './player/player.component';
import { SearchComponent } from './search/search.component';
import { FormsModule } from '@angular/forms';
import { SearchService } from './services/search.service';
import { Routes, RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ImageComponent } from './player/interactions/image/image.component';
import { PollComponent } from './player/interactions/poll/poll.component';
import { MatInputModule } from '@angular/material/input';
import { FirebaseService } from './services/firebase/firebase.service';
import { CommentComponent } from './player/interactions/comment/comment.component';
import { VideoComponent } from './player/interactions/video/video.component';

import { ClipboardModule } from '@angular/cdk/clipboard'
import { YoutubeComponent } from './player/interactions/youtube/youtube.component';
import { LinksComponent } from './player/interactions/links/links.component';
import { MatTooltipModule } from '@angular/material/tooltip';


const appRoutes: Routes = [
  { path: 'search', component: SearchComponent },
  { path: 'podcast', component: PlayerComponent },
  { path: '', pathMatch: 'full', redirectTo: '/search'},
  { path: '**', redirectTo: '/search' }
];

@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    SearchComponent,
    ImageComponent,
    PollComponent,
    CommentComponent,
    VideoComponent,
    LinksComponent,
    YoutubeComponent

    
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientJsonpModule,
    RouterModule.forRoot(appRoutes),
    NgxAudioPlayerModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
		AngularFirestoreModule,
    MatCardModule, MatToolbarModule, MatIconModule, MatRadioModule, MatCheckboxModule, MatSlideToggleModule, 
    MatButtonModule, MatInputModule, MatIconModule,
    HttpClientModule, ClipboardModule, MatTooltipModule

  ],
  entryComponents: [
    CommentComponent
  ],
  providers: [SearchService, FirebaseService ],
  bootstrap: [AppComponent],
  exports: [
    PlayerComponent
  ]
})
export class AppModule { }
