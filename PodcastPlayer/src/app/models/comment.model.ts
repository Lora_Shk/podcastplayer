export class Comment {
    userName: string = "User Name";
    date: Date = new Date();
    content: string;
    podcastId: string;
    docId: string;
    docPath: string = "";
}