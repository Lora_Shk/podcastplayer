// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBk55OfgBQnONazIr-drJYm8xLmlUPavKc",
    authDomain: "podreact.firebaseapp.com",
    databaseURL: "https://podreact.firebaseio.com",
    projectId: "podreact",
    storageBucket: "podreact.appspot.com",
    messagingSenderId: "37838248532",
    //TODO why do we need this
    // appId: "1:37838248532:web:04878ff700642a1f4e2bc1",
    // measurementId: "G-H4Z125W6V8"
    
  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
