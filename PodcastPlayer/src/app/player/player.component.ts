import { Component, ViewChild, AfterViewInit, OnInit, HostListener, ElementRef } from "@angular/core";
import { PodcastService } from "./../services/podcast.service";
import { PodcastFeed } from "./../models/podcast-feed.model";
import { ActivatedRoute } from "@angular/router";
import { PodcastEpisode } from "../models/podcast-episode.model";
import { InteractionService } from "../services/interaction.service";
import { EpisodeInteraction, Action, ActionType } from "../models/episode-interaction.model";

@Component({
  selector: "app-player",
  templateUrl: "./player.component.html",
  styleUrls: ["./player.component.css"]
})
export class PlayerComponent implements OnInit, AfterViewInit {
  feed: PodcastFeed;
  activeState = '';
  @ViewChild("player") player;

  title = "PodcastPlayer";
  msbapTitle = "Beispiel";
  msbapAudioUrl = "";
  shareurl = window.location.href;
  mobile = false;
  placeholder = false;
  
  currentEpisodeInterctions: EpisodeInteraction;
  currentActionType: ActionType;
  action = Action;
  currentTime: number;

  constructor(
    private route: ActivatedRoute,
    private podcastService: PodcastService,
    private interactionService: InteractionService,
) {}

  ngOnInit() {
    if (window.screen.width <= 768) { // 768px portrait
      this.mobile = true;
      
    }
    console.log(this.shareurl);
    this.route.queryParamMap.subscribe(params => {
      this.podcastService.getPodcast(params.get("url")).subscribe(feed => {
        this.feed = feed;
      });
    });
  }

  ngAfterViewInit(): void {
    this.player.player.nativeElement.addEventListener("timeupdate", () => {
      if(this.currentTime !== this.player.currentTime) {
          this.currentTime = this.player.currentTime;
          if(this.currentActionType?.endpoint == this.player.currentTime){
            this.closeInteraction();
          }
          
          this.onTimeChanged(this.currentTime);
          
      }
    });
  }

  onTimeChanged(currentTime: number) {
    console.log(currentTime);
    

    if (!this.currentEpisodeInterctions?.interaction) {
      this.placeholder = true;
      return;
      
    }

    let interactionAction = this.currentEpisodeInterctions.interaction[currentTime];
    if(interactionAction?.action === undefined) {
      
      return;
      
     
    }
    
    this.currentActionType = interactionAction;
    this.placeholder = false;
    console.log(this.currentActionType);
    
    
  }

  playEpisode(episode: PodcastEpisode) {
    
    
    this.msbapAudioUrl = episode.enclosure.link;


    this.interactionService
      .searchInteractionsForCurrentEpisode(this.msbapAudioUrl)
      .subscribe(episodeInteractionsResults => {
        this.currentEpisodeInterctions = episodeInteractionsResults[0];
        this.player.play();
        console.log("playing: " + episode.enclosure.link);
      });
      if (this.mobile == true) { // 768px portrait
        
      }
  }

  setStateAsActive(state) {
    this.activeState = state;
  }

  modalTrigger(){
    let modal = document.getElementById("myModal");
    modal.style.display = "block";
  }

  modalExit(){
    let modal = document.getElementById("myModal");
    modal.style.display = "none";
  }

  closeInteraction() {
    this.currentActionType = undefined;
    this.placeholder = true;
  }

}



