import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { ActionType } from 'src/app/models/episode-interaction.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-youtube',
  // template: '<div class="yt_box"><iframe [src]="link"></iframe></div>',
  templateUrl: './youtube.component.html',
  styleUrls: ['./youtube.component.css']
})


export class YoutubeComponent implements OnChanges {
  
  @Input() interactionActionType: ActionType;
  @Output() durationExpired = new EventEmitter<string>();
  link: any;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.link = this.sanitizer.bypassSecurityTrustResourceUrl(this.interactionActionType.url);
  }
}
