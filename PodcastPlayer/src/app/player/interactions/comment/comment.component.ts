import { Component, OnInit, ViewChild, ViewContainerRef, Input, OnChanges } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase/firebase.service';
import { Comment } from 'src/app/models/comment.model';
import { Observable } from 'rxjs';
import { PodcastFeed } from 'src/app/models/podcast-feed.model';



@Component({
  selector: 'comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit, OnChanges {
  @ViewChild('dynamic', { read: ViewContainerRef }) viewContainerRef: ViewContainerRef;
  @Input() feed: PodcastFeed;

  public comment: Comment;
  comments$: Observable<Comment[]>;

  COMMENTS_REF: string = "comments";
  COL_NODE: string;

  // viewReply: string;
  editComment: string;

  constructor(private fbService: FirebaseService) {
    this.comment = new Comment();
    // this.viewReply = "";
  }
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if(this.feed) {
      this.comments$ = this.fbService.colWithIds$(this.COL_NODE, ref => ref.where("podcastId", "==", this.feed.feed.url).orderBy('date', 'desc'));
    }
  }

  ngOnInit() {
    this.COL_NODE = `${this.comment.docPath}/${this.COMMENTS_REF}`;
  }

  onComment(comment: Comment) {
    comment.podcastId = this.feed.feed.url;
    this.fbService.add(comment, this.COL_NODE)
      .then(_ => {
        this.comment = new Comment();
      })
  }

  onEdit(comment: Comment) {
    this.fbService.update(comment.docPath, comment)
      .then(_ => {
        this.editComment = "";
      })
  }

  onDelete(comment: Comment) {
    this.fbService.delete(comment.docPath);
  }

  // onViewReply(comment: Comment) {
  //   this.dynComponent.addDynamicComponent(this.viewContainerRef, comment);
  // }

}
