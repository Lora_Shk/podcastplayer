import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ActionType } from 'src/app/models/episode-interaction.model';
import { timer } from 'rxjs/internal/observable/timer';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  constructor() { }

  @Input() interactionActionType: ActionType;
  @Output() durationExpired = new EventEmitter<string>();

  ngOnInit(): void {
    
  }
}
