import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { EpisodeInteraction } from "../models/episode-interaction.model";

@Injectable({
  providedIn: "root"
})
export class InteractionService {
  constructor(private db: AngularFirestore) {}

  searchInteractionsForCurrentEpisode(
    link: string
  ): Observable<EpisodeInteraction[]> {
    return this.db
      .collection<EpisodeInteraction>("test", ref =>
        ref.where("link", "==", link)
      )
      .valueChanges();
  }
}
