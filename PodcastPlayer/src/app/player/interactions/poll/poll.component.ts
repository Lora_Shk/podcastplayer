import { Component, OnInit, Input, Output, ViewChild } from '@angular/core';
import * as Survey from "survey-angular";
import * as widgets from "surveyjs-widgets";
import { ActionType } from 'src/app/models/episode-interaction.model';
import { EventEmitter } from '@angular/core';
import { timer } from 'rxjs';

@Component({
  selector: 'app-poll',
  template: '<div id="surveyContainer"></div>',
  styleUrls: ['./poll.component.css']
})
export class PollComponent implements OnInit{
  json: object;
  @ViewChild("player") player;
  currentTime: number;
  placeholder = false;

  static staticCloseInteraction: EventEmitter<any>;
 
  constructor() { 
    PollComponent.staticCloseInteraction = this.closeInteraction;
  }

  @Input() interactionActionType: ActionType;
  @Output() durationExpired = new EventEmitter<string>();
  @Output() closeInteraction = new EventEmitter<any>(); 

    ngOnInit(): void{
    var survey = new Survey.Model(surveyJSON);
    survey.onComplete.add(sendDataToServer);
    Survey.SurveyNG.render("surveyContainer", {model:survey});
  }

}

var surveyJSON = { title: "Wie ist eure Meinung dazu?", completeText: "Senden",  showCompletedPage: "false", pages: [
  { name:"page1", questions: [ 
      { type: "radiogroup", choices: [ "Ja", "Nein" ], name: "frameworkUsing",title: "Sollten sich soziale Medien gegen Rechtspopulisten stellen?" },
      { type: "comment", name: "mvm", hasOther: true, title: "Warum ist das deine Meinung?" }
   ]}
 ],
//  completedHtml : '<img class="popup" src="{{this.feed?.feed.image}}">'
}

function sendDataToServer(survey) {
  console.log(this);
  this.placeholder = true;
  var resultAsString = JSON.stringify(survey.data);
  var x = document.getElementById("surveyContainer");
  // x.style.display = "none";
  PollComponent.staticCloseInteraction.emit();
  //alert(resultAsString); send Ajax request to your web server.
}