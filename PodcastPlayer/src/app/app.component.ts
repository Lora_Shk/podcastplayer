import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Track } from 'ngx-audio-player';  

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

  

  // @ViewChild('player') player;

  // title = 'PodcastPlayer';
  // msbapTitle = 'Beispiel';
  // msbapAudioUrl = 'https://files.freemusicarchive.org/storage-freemusicarchive-org/music/WFMU/Broke_For_Free/Directionless_EP/Broke_For_Free_-_01_-_Night_Owl.mp3';
  // msaapDisplayVolumeControls = true;
  // msbapDisplayTitle = false;

  // msaapDisplayTitle = true;
  // msaapDisplayPlayList = true;
  // msaapPageSizeOptions = [2,4,6];

// msaapPlaylist: Track[] = [
//   {
//       title: 'Audio One Title',
//       link: 'https://files.freemusicarchive.org/storage-freemusicarchive-org/music/WFMU/Broke_For_Free/Directionless_EP/Broke_For_Free_-_01_-_Night_Owl.mp3'
//   },
//   {
//       title: 'Audio Two Title',
//       link: 'Link to Audio Two URL'
//   },
//   {
//       title: 'Audio Three Title',
//       link: 'Link to Audio Three URL'
//   },
// ];


  ngAfterViewInit(): void {
  //   this.player.player.nativeElement.addEventListener('timeupdate', () => {
  //     this.timeChanged();
  //   });
  }

  // timeChanged(){
  //   if(this.player.currentTime === 3){
  //     alert("3 seconds reached");
  //   }
  // }

  // onClick(){
  //   console.log(this.player.currentTime);
  // }
}
