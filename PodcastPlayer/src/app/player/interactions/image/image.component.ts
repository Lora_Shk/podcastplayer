import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { ActionType } from 'src/app/models/episode-interaction.model';
import { timer } from 'rxjs/internal/observable/timer';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnChanges {

  @Input() interactionActionType: ActionType;
  @Output() durationExpired = new EventEmitter<string>();
  link: any;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
  
    this.link = this.sanitizer.bypassSecurityTrustResourceUrl(this.interactionActionType.url);
  }
}
