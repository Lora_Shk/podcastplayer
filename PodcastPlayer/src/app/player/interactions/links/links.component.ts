import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { ActionType } from 'src/app/models/episode-interaction.model';
import { Clipboard } from "@angular/cdk/clipboard"
import { timer } from 'rxjs/internal/observable/timer';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.css']
})
export class LinksComponent implements OnChanges {
  @Input() interactionActionType: ActionType;
  @Output() durationExpired = new EventEmitter<string>();
  link: any;
  title: any;
  articleUrl: any;
  constructor(private clipboard: Clipboard, private sanitizer: DomSanitizer) { }

  
  value="Hallo";
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    // timer(this.interactionActionType.duration * 1000)
    // .subscribe(() => {
    //   this.durationExpired.emit('Passing upwards works!');
    // });

    this.link = this.sanitizer.bypassSecurityTrustResourceUrl(this.interactionActionType.url);
    this.title = this.interactionActionType.title;
    this.articleUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.interactionActionType.articleUrl);
  }
}
